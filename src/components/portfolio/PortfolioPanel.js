import React from 'react';
import Img from 'gatsby-image';
import { Flipped } from 'react-flip-toolkit';
import PropTypes from 'prop-types';
import Fade from 'react-reveal/Fade';

import './PortfolioPanel.css';

const PortfolioPanel = ({ img, className, uniqueFlipId, onClick, shouldFlip, wrapperStyle }) => (
    <Flipped flipId={uniqueFlipId} shouldInvert={shouldFlip(uniqueFlipId)}>
        <div
            role="presentation"
            className={'portfolio-panel-container '.concat(className)}
            onClick={() => onClick(uniqueFlipId)}
            style={wrapperStyle}
        >
            <Flipped inverseFlipid={uniqueFlipId} shouldFlip={shouldFlip(uniqueFlipId)}>
                <Fade ssrFadeout delay={300} duration={250} exit={false}>
                    <Flipped flipId={`thumbnail-${uniqueFlipId}`} shouldFlip={shouldFlip(uniqueFlipId)}>
                        <Img fluid={img} imgStyle={{ borderRadius: '15px' }} />
                    </Flipped>
                </Fade>
            </Flipped>
        </div>
    </Flipped>
);

PortfolioPanel.propTypes = {
    img: PropTypes.any.isRequired,
    className: PropTypes.string,
    wrapperStyle: PropTypes.object,
    uniqueFlipId: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    shouldFlip: PropTypes.func.isRequired,
};

PortfolioPanel.defaultProps = {
    className: '',
    wrapperStyle: {},
};

export default PortfolioPanel;
