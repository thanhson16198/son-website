import React, { PureComponent } from 'react';
import Img from 'gatsby-image';
import { Flipped } from 'react-flip-toolkit';
import Fade from 'react-reveal/Fade';
import { Container, Row, Col } from 'reactstrap';
import PropTypes from 'prop-types';

import './ExpandedPortfolioPanel.css';

class ExpandedPortfolioPanel extends PureComponent {
    constructor(props) {
        super(props);
        this.handleMouseHover = this.handleMouseHover.bind(this);
        this.state = {
            hoverIndex: 0,
        };
    }

    handleMouseHover(index) {
        this.setState({
            hoverIndex: index,
        });
    }

    render() {
        const { imgData, imgList, wrapperStyle, uniqueFlipId,
            onClick, heading, headingFooter, descriptionList,
        } = this.props;
        const { hoverIndex } = this.state;

        return (
            <Flipped
                flipId={uniqueFlipId}
            >
                <div
                    role="presentation"
                    className="expanded-panel-container"
                    onClick={() => onClick(uniqueFlipId)}
                    style={wrapperStyle}
                >
                    <Flipped inverseFlipId={uniqueFlipId}>
                        <div>
                            <div className="expanded-panel-heading">
                                {heading}
                                <small className="expanded-panel-heading-footer">
                                    <i>{headingFooter}</i>
                                </small>
                            </div>
                            <Fade ssrFadeout duration={500} delay={300} exit={false}>
                                <Container fluid>
                                    <Row noGutters>
                                        <Col xs="12" md="6">
                                            <Flipped flipId={`thumbnail-${uniqueFlipId}`}>
                                                <div>
                                                    <Img fluid={imgData[imgList[hoverIndex]].childImageSharp.fluid} imgStyle={{ borderRadius: '15px' }} />
                                                    <hr className="styled d-none d-md-block" />
                                                    <Container fluid>
                                                        <Row>
                                                            {imgList.map((imgName, index) => (
                                                                <Col sm={6} lg={4} className="d-none d-md-block" key={imgName}>
                                                                    <div
                                                                        onMouseEnter={() => this.handleMouseHover(index)}
                                                                    >
                                                                        <Img
                                                                            className={index === hoverIndex ? 'expanded-panel-img-active' : 'expanded-panel-img'}
                                                                            fluid={imgData[imgName].childImageSharp.fluid}
                                                                        />
                                                                    </div>
                                                                </Col>
                                                            ))}
                                                        </Row>
                                                    </Container>
                                                </div>
                                            </Flipped>
                                        </Col>
                                        <Col xs="12" md="6">
                                            <Flipped flipId={`description-${uniqueFlipId}`}>
                                                <div className="expanded-panel-details">
                                                    {descriptionList.map((section, index) => (
                                                        <div
                                                            key={`${uniqueFlipId}-${section.heading}`}
                                                        >
                                                            <strong>{section.heading}</strong>
                                                            {section.heading === 'Responsibilities' ? (
                                                                <ul className="expanded-panel-description">
                                                                    {section.description}
                                                                </ul>
                                                            ) : (
                                                                <div className="expanded-panel-description">
                                                                    {section.description}
                                                                </div>
                                                            )}
                                                        </div>
                                                    ))}
                                                </div>
                                            </Flipped>
                                        </Col>
                                    </Row>
                                </Container>
                            </Fade>
                        </div>
                    </Flipped>
                </div>
            </Flipped>
        );
    }
}

ExpandedPortfolioPanel.propTypes = {
    imgData: PropTypes.any.isRequired,
    imgList: PropTypes.arrayOf(PropTypes.string).isRequired,
    wrapperStyle: PropTypes.object,
    uniqueFlipId: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    heading: PropTypes.any.isRequired,
    headingFooter: PropTypes.any.isRequired,
    descriptionList: PropTypes.arrayOf(PropTypes.object).isRequired,
};

ExpandedPortfolioPanel.defaultProps = {
    wrapperStyle: {},
};

export default ExpandedPortfolioPanel;
