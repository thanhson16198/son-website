import React, { PureComponent, Fragment } from 'react';
import { StaticQuery, graphql } from 'gatsby';
import { Parallax, ParallaxLayer } from 'react-spring/addons';
import { Flipper } from 'react-flip-toolkit';

import PortfolioHeading from './PortfolioHeading';
import PortfolioPanel from './PortfolioPanel';
import ExpandedPortfolioPanel from './ExpandedPortfolioPanel';
import PortfolioFooter from './PortfolioFooter';

import WaveUrl from '../../compressed_images/wave.svg';
import CloudUrl from '../../compressed_images/cloud.svg';
import DivingUrl from '../../compressed_images/diving.svg';
import FishLeftBtmUrl from '../../compressed_images/fish_leftbottom.svg';
import TurtleGreyUrl from '../../compressed_images/turtle_1.svg';
import TurtleGreenUrl from '../../compressed_images/turtle.svg';
import BubblesBlueUrl from '../../compressed_images/bubbles_blue.svg';
import DyingScubaDiver from '../../compressed_images/scuba-diving.svg';
import ScubaDiver from '../../compressed_images/snorkel_1.svg';
import SubmarineUrl from '../../compressed_images/submarine.svg';

import './PortfolioScrollParallax.css';
import './PortfolioPanel.css';
import './ExpandedPortfolioPanel.css';

export const fluidImg = graphql`
  fragment fluidImg on File {
    childImageSharp {
      fluid(maxWidth: 960) {
        ...GatsbyImageSharpFluid_tracedSVG
      }
    }
  }
`;

const portfolioQuery = graphql`
  query {
      classbuzz0: file(relativePath: { eq: "classbuzz_0.png" }) {
          ...fluidImg
      }
      classbuzz1: file(relativePath: { eq: "classbuzz_1.png" }) {
          ...fluidImg
      }
      classbuzz2: file(relativePath: { eq: "classbuzz_2.png" }) {
          ...fluidImg
      }
      classbuzz3: file(relativePath: { eq: "classbuzz_3.png" }) {
          ...fluidImg
      }
      classbuzz4: file(relativePath: { eq: "classbuzz_4.png" }) {
          ...fluidImg
      }
      classbuzz5: file(relativePath: { eq: "classbuzz_5.png" }) {
          ...fluidImg
      }

      innosenze0: file(relativePath: { eq: "innosenze_0.png" }) {
          ...fluidImg
      }
      innosenze1: file(relativePath: { eq: "innosenze_1.png" }) {
          ...fluidImg
      }
      innosenze2: file(relativePath: { eq: "innosenze_2.png" }) {
          ...fluidImg
      }
      innosenze3: file(relativePath: { eq: "innosenze_3.png" }) {
          ...fluidImg
      }

      taskrr0: file(relativePath: { eq: "taskrr0.png" }) {
          ...fluidImg
      }
      taskrr1: file(relativePath: { eq: "taskrr1.png" }) {
          ...fluidImg
      }
      taskrr2: file(relativePath: { eq: "taskrr2.png" }) {
          ...fluidImg
      }
      taskrr3: file(relativePath: { eq: "taskrr3.png" }) {
          ...fluidImg
      }
      taskrr4: file(relativePath: { eq: "taskrr4.png" }) {
          ...fluidImg
      }
  }
`;

const classbuzzImgs = [
    'classbuzz0', 'classbuzz1', 'classbuzz2', 'classbuzz3', 'classbuzz4', 'classbuzz5',
];

const innosenzeImgs = [
    'innosenze0', 'innosenze1', 'innosenze2', 'innosenze3',
];

const taskrrImgs = [
    'taskrr0', 'taskrr1', 'taskrr2', 'taskrr3', 'taskrr4',
];

const classbuzzDetails = {
    heading:
        <h4>
            Class
            <b>Buzz</b>
        </h4>,
    heading_footer: 'A Social Networking App For Kids',
    descriptionList: [
        {
            heading: 'Award',
            description: '2nd Prize - CS3216, NUS STePS 13th, 2018',
        },
        {
            heading: 'Description',
            description: 'Social Networking App for Kids serve as an experiential learning platform for students to learn Cyber Wellness concepts in an authentic context.',
        },
        {
            heading: 'Technologies used',
            description: 'ReactJS, Redux, Django, PostgreSQL',
        },
        {
            heading: 'Organization',
            description: 'NUS, supported by Ministry of Education.',
        },
        {
            heading: 'Period',
            description: '10/2018 - Present',
        },
        {
            heading: 'Role',
            description: 'Back-end Developer',
        },
        {
            heading: 'Responsibilities',
            description:
                <Fragment>
                    <li>Database schema design and integration.</li>
                    <li>RESTful API design.</li>
                    <li>Access and permission controls.</li>
                    <li>Server-side development.</li>
                </Fragment>
            ,
        },
    ],
};

const innosenzeDetails = {
    heading:
        <h4>
            Backend SE Internship
        </h4>,
    heading_footer: 'Data Pre-Processing for USPTO & WIPO patent databases',
    descriptionList: [
        {
            heading: 'Description',
            description: 'Developed an automated system to extract data from public datasets, or web scraping, and perform data extracting, cleaning and transfering to MongoDB.',
        },
        {
            heading: 'Technologies used',
            description: 'Python, MongoDB, Selenium, BeautifulSoup4, LXML',
        },
        {
            heading: 'Organization',
            description: 'InnoSenze Pte. Ltd.',
        },
        {
            heading: 'Period',
            description: '06/2018 - 08/2018',
        },
        {
            heading: 'Role',
            description: 'Back-end Developer',
        },
        {
            heading: 'Responsibilities',
            description:
                <Fragment>
                    <li>Develop an automated system for data pre-processing.</li>
                    <li>Extract and clean GBs of XML files into MongoDB.</li>
                    <li>Perform web scraping for data interpretation.</li>
                </Fragment>
            ,
        },
    ],
};

const taskrrDetails = {
    heading:
        <h4>
            Taskrr
        </h4>,
    heading_footer: 'A Task Sourcing Web Application',
    descriptionList: [
        {
            heading: 'Description',
            description: 'A task matching web application, to facilitate users to hire temporary help.',
        },
        {
            heading: 'Technologies used',
            description: 'NodeJS, Bootstrap, PostgreSQL, PyTest',
        },
        {
            heading: 'Organization',
            description: 'CS2102, NUS',
        },
        {
            heading: 'Period',
            description: '01/2018 - 04/2018',
        },
        {
            heading: 'Role',
            description: 'SQL Developer | Software Tester',
        },
        {
            heading: 'Responsibilities',
            description:
                <Fragment>
                    <li>Database schema and model design, maintenance and deployment.</li>
                    <li>Wrote SQL queries for database integration on NodeJS.</li>
                    <li>Develop a search algorithm on tasks&apos; details.</li>
                    <li>Implement automated testing on functionalities of the web app.</li>
                </Fragment>
            ,
        },
    ],
};

const shouldFlip = index => (prevDecisionData, currentDecisionData) => index === prevDecisionData || index === currentDecisionData;

class PortfolioScrollParallax extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            width: 0,
            focusedId: null,
            loading: true,
        };
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.renderTopWaves = this.renderTopWaves.bind(this);
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
        setTimeout(() => this.setState({ loading: false }), 500);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions = () => {
        this.setState({
            width: window.innerWidth,
        });
    }

    onClick = (id) => {
        this.setState(prevState => ({
            focusedId: prevState.focusedId === id ? null : id,
        }));
    }

    renderTopWaves() {
        const { width } = this.state;
        const numOfWaves = Math.ceil(width / 175);
        const waves = [];
        for (let i = 0; i < numOfWaves; i += 1) {
            waves.push(
                <img
                    src={WaveUrl}
                    key={'wave'.concat(i)}
                    alt={'wave'.concat(i)}
                    style={{ overflowY: 'hidden' }}
                />,
            );
        }

        return (
            <div style={{ display: 'flex' }}>
                {waves}
            </div>
        );
    }

    render() {
        const { loading } = this.state;
        if (loading) {
            return (
                <div className="preloader-background" style={{ height: '100vh', width: '100%' }}>
                    <div className="preloader-portfolio">
                        <span />
                        <span />
                        <span />
                        <span />
                        <span />
                    </div>
                </div>
            );
        }
        const { focusedId } = this.state;
        return (
            <StaticQuery
                query={portfolioQuery}
                render={data => (

                <Parallax ref={(ref) => { this.parallax = ref; }} pages={6.2}>
                    {/* Blue sky */}
                    <ParallaxLayer offset={0} speed={-0.3} className="sky-gradient" style={{ maxHeight: '90vh' }} />

                    {/* Heading */}
                    <ParallaxLayer offset={0.3} speed={-0.2} style={{ height: 'auto' }}>
                        <PortfolioHeading />
                    </ParallaxLayer>

                    {/* Sea Levels */}
                    <ParallaxLayer offset={0.8} speed={0} factor={1.5} style={{ pointerEvents: 'none' }} className="sea-level-1" />
                    <ParallaxLayer offset={2.3} speed={0} factor={1.5} style={{ pointerEvents: 'none' }} className="sea-level-2" />
                    <ParallaxLayer offset={3.8} speed={0} factor={2.5} style={{ pointerEvents: 'none' }} className="sea-level-3" />

                    {/* TopWaves */}
                    <ParallaxLayer offset={0.75} speed={0} style={{ pointerEvents: 'none' }}>
                        {this.renderTopWaves()}
                    </ParallaxLayer>

                    {/* Clouds */}
                    <ParallaxLayer offset={0.05} speed={0.2} style={{ opacity: 0.5, height: 'auto', pointerEvents: 'none' }}>
                        <img src={CloudUrl} alt="cloud" style={{ display: 'block', width: '12%', marginLeft: '15%' }} />
                        <img src={CloudUrl} alt="cloud" style={{ display: 'block', width: '15%', marginLeft: '80%' }} />
                    </ParallaxLayer>
                    <ParallaxLayer offset={0.3} speed={-0.1} style={{ opacity: 0.6, height: 'auto', pointerEvents: 'none' }}>
                        <img src={CloudUrl} alt="cloud" style={{ display: 'block', width: '10%', marginLeft: '5%' }} />
                        <img src={CloudUrl} alt="cloud" style={{ display: 'block', width: '10%', marginLeft: '82%' }} />
                    </ParallaxLayer>
                    <ParallaxLayer offset={0.4} speed={0.4} style={{ opacity: 0.3, height: 'auto', pointerEvents: 'none' }}>
                        <img src={CloudUrl} alt="cloud" style={{ display: 'block', width: '12%', marginLeft: '75%' }} />
                        <img src={CloudUrl} alt="cloud" style={{ display: 'block', width: '15%', marginLeft: '15%' }} />
                    </ParallaxLayer>

                    {/* Divers */}
                    <ParallaxLayer offset={1.2} speed={0.2} style={{ display: 'block', marginLeft: '85%', opacity: 0.5, height: 'auto', pointerEvents: 'none' }}>
                        <img src={DivingUrl} alt="snorkel1" style={{ width: '10%' }} className="flip-horizontal" />
                    </ParallaxLayer>
                    <ParallaxLayer offset={1.25} speed={0.4} style={{ display: 'block', marginLeft: '63%', opacity: 0.4, height: 'auto' }}>
                        <div className="talk-bubble tri-right right-top">
                            <div className="talktext">
                                <div>Ongoing</div>
                                <p>A Social Networking App for students to learn Cyber Wellness concepts.</p>
                            </div>
                        </div>
                    </ParallaxLayer>
                    <ParallaxLayer offset={2.5} speed={0.6} style={{ display: 'block', marginLeft: '10%', opacity: 0.4, height: 'auto', pointerEvents: 'none' }}>
                        <img src={DyingScubaDiver} alt="scuba-diver" style={{ width: '10%' }} />
                    </ParallaxLayer>
                    <ParallaxLayer offset={2.55} speed={0.4} style={{ display: 'block', marginLeft: '23%', opacity: 0.4, height: 'auto' }}>
                        <div className="talk-bubble tri-right left-top">
                            <div className="talktext">
                                <div>11/2018</div>
                                <p>
                                    Data Pre-Processing for USPTO & WIPO patent databases.
                                </p>
                            </div>
                        </div>
                    </ParallaxLayer>
                    <ParallaxLayer offset={4} speed={0.2} style={{ display: 'block', marginLeft: '80%', opacity: 0.4, height: 'auto', pointerEvents: 'none' }}>
                        <img src={SubmarineUrl} alt="submarine" style={{ width: '14%' }} className="flip-horizontal" />
                    </ParallaxLayer>
                    <ParallaxLayer offset={4} speed={0.4} style={{ display: 'block', marginLeft: '58%', opacity: 0.4, height: 'auto' }}>
                        <div className="talk-bubble tri-right right-top">
                            <div className="talktext">
                                <div>04/2018</div>
                                <p>
                                    A Task Sourcing Web Application
                                </p>
                            </div>
                        </div>
                    </ParallaxLayer>

                    {/* Fishes */}
                    <ParallaxLayer offset={1.9} speed={0.5} style={{ display: 'block', marginLeft: '75%', opacity: 0.2, height: 'auto', pointerEvents: 'none' }}>
                        <img src={TurtleGreyUrl} alt="turtle" style={{ width: '10%' }} className="flip-horizontal" />
                    </ParallaxLayer>
                    <ParallaxLayer offset={2} speed={1} style={{ display: 'block', marginLeft: '20%', opacity: 0.4, height: 'auto', pointerEvents: 'none' }}>
                        <img src={FishLeftBtmUrl} alt="fish" style={{ width: '5%' }} className="flip-horizontal" />
                    </ParallaxLayer>
                    <ParallaxLayer offset={3.4} speed={0.4} style={{ display: 'block', marginLeft: '15%', opacity: 0.3, height: 'auto', pointerEvents: 'none' }}>
                        <img src={TurtleGreenUrl} alt="turtle" style={{ width: '9%' }} />
                    </ParallaxLayer>

                    {/* Decorators */}
                    <ParallaxLayer offset={1} speed={0.5} style={{ display: 'block', marginLeft: '80%', opacity: 0.8, height: 'auto', pointerEvents: 'none' }}>
                        <img src={BubblesBlueUrl} alt="bubbles" style={{ width: '5%' }} />
                    </ParallaxLayer>
                    <ParallaxLayer offset={1.9} speed={0.4} style={{ height: 'auto', pointerEvents: 'none' }}>
                        <img src={BubblesBlueUrl} alt="bubbles" style={{ display: 'block', width: '10%', marginLeft: '20%', opacity: 1 }} className="flip-horizontal" />
                        <img src={BubblesBlueUrl} alt="bubbles" style={{ display: 'block', width: '12%', marginLeft: '65%', opacity: 0.5 }} />
                    </ParallaxLayer>
                    <ParallaxLayer offset={2} speed={1.5} style={{ display: 'block', marginLeft: '80%', opacity: 0.4, height: 'auto', pointerEvents: 'none' }}>
                        <img src={BubblesBlueUrl} alt="bubbles" style={{ width: '12%' }} />
                    </ParallaxLayer>
                    <ParallaxLayer offset={2.95} speed={0.5} style={{ height: 'auto', pointerEvents: 'none' }}>
                        <img src={BubblesBlueUrl} alt="bubbles" style={{ display: 'block', width: '12%', marginLeft: '65%', opacity: 0.4 }} />
                        <img src={BubblesBlueUrl} alt="bubbles" style={{ display: 'block', width: '7%', marginLeft: '20%', opacity: 0.3 }} className="flip-horizontal" />
                        <img src={ScubaDiver} alt="scuba-diver" style={{ display: 'block', width: '12%', marginLeft: '50%', opacity: 0.2 }} />
                    </ParallaxLayer>
                    <ParallaxLayer offset={3.2} speed={0.1} style={{ height: 'auto', pointerEvents: 'none' }}>
                        <img src={BubblesBlueUrl} alt="bubbles" style={{ display: 'block', width: '7%', marginLeft: '5%', opacity: 0.5 }} />
                        <img src={BubblesBlueUrl} alt="bubbles" style={{ display: 'block', width: '10%', marginLeft: '70%', opacity: 0.2 }} className="flip-horizontal" />
                    </ParallaxLayer>
                    <ParallaxLayer offset={3.7} speed={1.3} style={{ height: 'auto', pointerEvents: 'none' }}>
                        <img src={BubblesBlueUrl} alt="bubbles" style={{ display: 'block', width: '15%', marginLeft: '30%', opacity: 0.7 }} />
                    </ParallaxLayer>
                    <ParallaxLayer offset={4.9} speed={0.2} style={{ height: 'auto', pointerEvents: 'none' }}>
                        <img src={BubblesBlueUrl} alt="bubbles" style={{ display: 'block', width: '7%', marginLeft: '5%', opacity: 0.3 }} />
                        <img src={BubblesBlueUrl} alt="bubbles" style={{ display: 'block', width: '10%', marginLeft: '70%', opacity: 0.2 }} className="flip-horizontal" />
                    </ParallaxLayer>
                    <ParallaxLayer offset={5} speed={0.8} style={{ height: 'auto', pointerEvents: 'none' }}>
                        <img src={BubblesBlueUrl} alt="bubbles" style={{ display: 'block', width: '8%', marginLeft: '30%', opacity: 0.4 }} />
                    </ParallaxLayer>

                    {/* Footer */}
                    <ParallaxLayer offset={5.5} speed={0} style={{ display: 'block', height: 'auto' }}>
                        <PortfolioFooter />
                    </ParallaxLayer>

                    {/* Panels */}
                    <ParallaxLayer offset={1.2} speed={0} style={{ display: 'block', width: '50%', marginLeft: '10%' }}>
                        <Flipper
                            flipKey={focusedId}
                            decisionData={focusedId}
                        >
                            {
                                focusedId === 'classbuzz' ? (
                                    <ExpandedPortfolioPanel
                                        thumbnailImg={classbuzzImgs[0]}
                                        imgList={classbuzzImgs}
                                        imgData={data}
                                        uniqueFlipId="classbuzz"
                                        onClick={this.onClick}
                                        heading={classbuzzDetails.heading}
                                        headingFooter={classbuzzDetails.heading_footer}
                                        descriptionList={classbuzzDetails.descriptionList}
                                    />
                                ) : (
                                    <PortfolioPanel
                                        img={data.classbuzz0.childImageSharp.fluid}
                                        uniqueFlipId="classbuzz"
                                        onClick={this.onClick}
                                        shouldFlip={shouldFlip}
                                    />
                                )
                            }
                        </Flipper>
                    </ParallaxLayer>
                    <ParallaxLayer offset={2.5} speed={0} style={{ display: 'block', marginLeft: '10%' }}>
                        <Flipper
                            flipKey={focusedId}
                            decisionData={focusedId}
                            staggerConfig={{
                                card: {
                                    reverse: focusedId !== null,
                                    speed: 0.5,
                                },
                            }}
                        >
                            {
                                focusedId === 'innosenze' ? (
                                    <ExpandedPortfolioPanel
                                        thumbnailImg={innosenzeImgs[0]}
                                        imgList={innosenzeImgs}
                                        imgData={data}
                                        uniqueFlipId="innosenze"
                                        onClick={this.onClick}
                                        heading={innosenzeDetails.heading}
                                        headingFooter={innosenzeDetails.heading_footer}
                                        descriptionList={innosenzeDetails.descriptionList}
                                    />
                                ) : (
                                    <PortfolioPanel
                                        img={data.innosenze0.childImageSharp.fluid}
                                        uniqueFlipId="innosenze"
                                        onClick={this.onClick}
                                        shouldFlip={shouldFlip}
                                        wrapperStyle={{
                                            width: '45%',
                                            marginLeft: '40%',
                                        }}
                                    />
                                )
                            }
                        </Flipper>
                    </ParallaxLayer>
                    <ParallaxLayer offset={4} speed={0} style={{ display: 'block', width: '50%', marginLeft: '5%' }}>
                        <Flipper
                            flipKey={focusedId}
                            decisionData={focusedId}
                            staggerConfig={{
                                card: {
                                    reverse: focusedId !== null,
                                    speed: 0.5,
                              },
                            }}
                        >
                            {
                                focusedId === 'taskrr' ? (
                                    <ExpandedPortfolioPanel
                                        thumbnailImg={taskrrImgs[0]}
                                        imgList={taskrrImgs}
                                        imgData={data}
                                        uniqueFlipId="taskrr"
                                        onClick={this.onClick}
                                        heading={taskrrDetails.heading}
                                        headingFooter={taskrrDetails.heading_footer}
                                        descriptionList={taskrrDetails.descriptionList}
                                    />
                                ) : (
                                    <PortfolioPanel
                                        img={data.taskrr0.childImageSharp.fluid}
                                        uniqueFlipId="taskrr"
                                        onClick={this.onClick}
                                        shouldFlip={shouldFlip}
                                    />
                                )
                            }
                        </Flipper>
                    </ParallaxLayer>
                </Parallax>
            )}
            />
        );
    }
}

export default PortfolioScrollParallax;
