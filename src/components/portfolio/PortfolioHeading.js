import React from 'react';

import './PortfolioHeading.css';

const PortfolioHeading = () => (
    <div className="portfolio-heading-container">
        <span className="portfolio-mini-heading">
            Son Nguyen&apos;s
        </span>
        <h4 className="portfolio-heading">
            Portfolio
        </h4>
    </div>
);

export default PortfolioHeading;
