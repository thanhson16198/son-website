import React from 'react';
import { faSmile } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import ContactIcons from '../common/ContactIcons';
import './PortfolioFooter.css';

const PortfolioFooter = () => (
    <div className="portfolio-footer-container">
        <p className="portfolio-footer">
            Now you know some of the projects I&apos;ve done,
             please feel free to reach out and have a chat&nbsp;
            <FontAwesomeIcon icon={faSmile} />
        </p>
        <div className="portfolio-footer-icons">
            <ContactIcons placement="bottom" blockId={3} size="2x" />
        </div>
        <div style={{
                marginTop: '40px',
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-around',
            }}
        >
            <a
                className="nav-link pointer-box left"
                style={{ textDecoration: 'none', padding: 0, margin: 0 }}
                href="https://snguyenthanh.netlify.com"
                target="_blank"
                rel="noopener noreferrer"
            >
                <div className="pointer-box-text left">
                    <small>Go to</small>
                    <br />
                    <strong>Homepage</strong>
                </div>
            </a>
            <a
                className="nav-link pointer-box right"
                style={{ textDecoration: 'none', padding: 0, margin: 0 }}
                href="https://snguyenthanh.netlify.com/resume"
                target="_blank"
                rel="noopener noreferrer"
            >
                <div className="pointer-box-text right">
                    <small>Check out</small>
                    <br />
                    <strong>Resume</strong>
                </div>
            </a>
        </div>
    </div>
);

export default PortfolioFooter;
