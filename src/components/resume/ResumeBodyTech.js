import React from 'react';
import { Container, Row, Col } from 'reactstrap';

const dayToDayTechs = [
    'Python / Django / Flask',
    'ReactJS / GatsbyJS',
    'Material-UI / Bootstrap / Bulma / Plotly',
    'PostgreSQL / MariaDB / MongoDB',
    'AWS / Firebase',
    'PyTest / Unittest / Travis CI / Jenkins',
    'Version Control ( GIT )',
    'Responsive Layout and Design',
];

const experienceTechs = [
    'Java / C',
    'Redux / React Native',
    'WebSocket / Protobuf',
    'Tor',
    'Selenium',
    'BeautifulSoup4 / LXML',
];

const ResumeBodyTech = () => (
    <Container fluid style={{ marginBottom: '70px' }}>
        <Row noGutters>
            <Col xs="12" sm={{ size: 3, offset: 1 }}>
                <h3 className="tech-section-title">
                    Technologies
                </h3>
            </Col>
            <Col xs="12" sm="4">
                <h3 className="tech-heading">
                    DAY-TO-DAY USAGE
                </h3>
                <ul className="tech-text-container">
                    {dayToDayTechs.map(tech => (
                        <li key={tech}>{tech}</li>
                    ))}
                </ul>
            </Col>
            <Col xs="12" sm="4">
                <h3 className="tech-heading">
                    EXPERIENCE WITH
                </h3>
                <ul className="tech-text-container">
                    {experienceTechs.map(tech => (
                        <li key={tech}>{tech}</li>
                    ))}
                </ul>
            </Col>
        </Row>
    </Container>
);

export default ResumeBodyTech;
