import React from 'react';

import SEO from '../components/seo';
import PortfolioScrollParallax from '../components/portfolio/PortfolioScrollParallax';
import 'bootstrap/dist/css/bootstrap.min.css'; // Bootstrap 4.2.1
import '../css/fonts.css';

const PortfolioPage = () => (
  <div>
    <SEO title="Portfolio - Son Nguyen" keywords={['snguyenthanh', 'portfolio', 'Son Nguyen']} />
    <PortfolioScrollParallax />
  </div>
);

export default PortfolioPage;
