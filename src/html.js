import React from "react";
import PropTypes from "prop-types";
import { Helmet } from "react-helmet";

const googleAnalyticsScript = "window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-132818221-1');";

const HTML = ({ htmlAttributes, headComponents, bodyAttributes,
    preBodyComponents, body, postBodyComponents }) => (
      <html {...htmlAttributes}>
        <head>
          <meta charSet="utf-8" />
          <meta httpEquiv="x-ua-compatible" content="ie=edge" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no"
          />
          {headComponents}
        </head>
        <body {...bodyAttributes}>
          {preBodyComponents}
          <div
            key={`body`}
            id="___gatsby"
            dangerouslySetInnerHTML={{ __html: body }}
          />
          {postBodyComponents}
          <Helmet>
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-132818221-1" />
            <script>
              {googleAnalyticsScript}
            </script>
          </Helmet>
        </body>
      </html>
);

export default HTML;

HTML.propTypes = {
  htmlAttributes: PropTypes.object,
  headComponents: PropTypes.array,
  bodyAttributes: PropTypes.object,
  preBodyComponents: PropTypes.array,
  body: PropTypes.string,
  postBodyComponents: PropTypes.array,
};
